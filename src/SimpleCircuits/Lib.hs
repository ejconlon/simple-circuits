{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module SimpleCircuits.Lib where

import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.List (find)
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Maybe (isJust)
import qualified Data.Set as Set
import Data.Set (Set)

newtype Ohms = Ohms Double deriving (Show, Eq, Ord, Num)
newtype Potential = Potential Double deriving (Show, Eq, Ord, Num)
newtype Amps = Amps Double deriving (Show, Eq, Ord, Num)

data Elt =
    Resistor { resistance :: Ohms }
  | VoltSource { potential :: Potential }
  deriving (Show, Eq)

newtype EltId = EltId Int deriving (Show, Eq, Ord, Num)
newtype WireId = WireId Int deriving (Show, Eq, Ord, Num)

data Wire = Wire EltId EltId deriving (Show)

isWireAt :: EltId -> Wire -> Bool
isWireAt i (Wire a b) = i == a || i == b

instance Eq Wire where
  (Wire a b) == (Wire c d) = (a == c && b == d) || (a == d && b == c)

instance Ord Wire where
  (Wire a b) <= (Wire c d) =
    let
      a' = min a b
      b' = max a b
      c' = min c d
      d' = max c d
    in
      (a' < c') || (a' == c' && b' <= d')

data CircuitData e =
  CircuitData {
    eltIdSource :: EltId,
    elts :: Map EltId e,
    wires :: Set Wire
  } deriving (Show, Eq)

data Join = Join deriving (Show, Eq)

data CircuitError =
    NoElt EltId
  | AlreadyWire Wire
  | NoWire Wire
  deriving (Show, Eq)

newtype Circuit e a =
  Circuit {
    runCircuit :: ExceptT CircuitError (State (CircuitData e)) a
  } deriving (Functor, Applicative, Monad, MonadState (CircuitData e), MonadError CircuitError)

emptyCircuitData :: CircuitData e
emptyCircuitData =
  CircuitData {
    eltIdSource = EltId 0,
    elts = Map.empty,
    wires = Set.empty
  }

addElt :: e -> Circuit e EltId
addElt elt = do
  dat <- get
  let eltId = eltIdSource dat
      nextDat =
        dat {
          eltIdSource = eltId + 1,
          elts = Map.insert eltId elt (elts dat)
        }
  put nextDat
  return eltId

hasElt :: EltId -> Circuit e Bool
hasElt eltId = isJust <$> lookupElt eltId

getElt :: EltId -> Circuit e e
getElt eltId = do
  maybeElt <- lookupElt eltId
  case maybeElt of
    Just elt -> return elt
    Nothing -> throwError $ NoElt eltId

lookupElt :: EltId -> Circuit e (Maybe e)
lookupElt eltId = do
  dat <- get
  return $ Map.lookup eltId (elts dat)

wiresAt :: EltId -> Circuit e (Set Wire)
wiresAt eltId = do
  dat <- get
  return $ Set.filter (isWireAt eltId) (wires dat)

hasWire :: Wire -> Circuit e Bool
hasWire w = do
  dat <- get
  return $ Set.member w (wires dat)

addWire :: Wire -> Circuit e ()
addWire w = do
  dat <- get
  put dat { wires = Set.insert w (wires dat) }

circuitValue :: Circuit e a -> Either CircuitError a
circuitValue c = h
  where
    d = runCircuit c
    e = runExceptT d
    f = runStateT e emptyCircuitData
    g = runIdentity f
    h = fst g

data Element = SingleElement EltId | ParallelElement Parallel

data Series = Series [Element]

data Parallel = Parallel [Series]

myForM_ :: (Monad m) => [a] -> (a -> m b) -> m ()
myForM_ [] _ = return ()
myForM_ (x:xs) f = f x >> myForM_ xs f

pairs :: [a] -> [(a, a)]
pairs [] = []
pairs (a:[]) = []
pairs (a:b:cs) = (a, b):(pairs (b:cs))

{-
Examples:

Serial:

do
  v <- addElt $ Voltage 60
  r1 <- addElt $ Resistor 17
  r2 <- addElt $ Resistor 12
  r3 <- addElt $ Resistor 11
  let s = Series [SingleElement v, SingleElement r1, SingleElement r2, SingleElement r3, SingleElement v]
  wireSeries s

Parallel:

do
  v <- addElt $ Voltage 60
  r1 <- addElt $ Resistor 5
  r2 <- addElt $ Resistor 10
  r3 <- addElt $ Resistor 15
  let p = Parallel [Series [SingleElement r1], Series [SingleElement r2], Series [SingleElement r3]]
  let s = Series [SingleElement v, ParallelElement p, SingleElement v]
  wireSeries s
-}
